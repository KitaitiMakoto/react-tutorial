import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Square(props) {
  return (
    <button className="square" onClick={props.onClick}>
      {props.value}
    </button>
  );
}

class Board extends React.Component {
  renderSquare(i) {
    return (
      <Square
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
      />
    );
  }

  render() {
    return (
      <div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
      }],
      stepNumber: 0,
      xIsNext: true,
    };
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({
      history: history.concat([{
        squares: squares,
      }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    });
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
    });
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = calculateWinner(current.squares);

    const moves = history.map((step, move) => {
      const desc = move ?
            'Go to move #' + move :
            'Go to game start';
      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      );
    });

    let status;
    if (winner) {
      status = 'Winner: ' + winner;
    } else {
      status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            onClick={(i) => this.handleClick(i)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

const PRODUCTS = [
  {category: "Sporting Goods", price: "$49.99", stocked: true, name: "Football"},
  {category: "Sporting Goods", price: "$9.99", stocked: true, name: "Baseball"},
  {category: "Sporting Goods", price: "$29.99", stocked: false, name: "Basketball"},
  {category: "Electronics", price: "$99.99", stocked: true, name: "iPod Touch"},
  {category: "Electronics", price: "$399.99", stocked: false, name: "iPhone 5"},
  {category: "Electronics", price: "$199.99", stocked: true, name: "Nexus 7"}
];

class FilterableProductTable extends React.Component {
  constructor(props) {
    super(props);
    this.handleQueryChange = this.handleQueryChange.bind(this);
    this.handleShowOnlyInStock = this.handleShowOnlyInStock.bind(this);
    this.state = {
      query: '',
      showOnlyInStock: false,
    };
  }

  handleQueryChange(event) {
    this.setState({
      query: event.target.value,
    });
  }

  handleShowOnlyInStock(event) {
    this.setState({
      showOnlyInStock: event.target.checked,
    });
  }

  render() {
    return (
      <div>
        <SearchBar
          query={this.state.query}
          handleQueryChange={this.handleQueryChange}
          showOnlyInStock={this.state.showOnlyInStock}
          handleShowOnlyInStock={this.handleShowOnlyInStock}
        />
        <ProductTable query={this.state.query} showOnlyInStock={this.state.showOnlyInStock} />
      </div>
    );
  }
}

class SearchBar extends React.Component {
  render() {
    return (
      <div>
        <input type="search" value={this.props.query} onChange={this.props.handleQueryChange} />
        <label><input type="checkbox" checked={this.props.showOnlyInStock} onChange={this.props.handleShowOnlyInStock} />Only show products in stock</label>
      </div>
    );
  }
}

class ProductTable extends React.Component {
  render() {
    const categories = {};
    for (let product of PRODUCTS) {
      if (! categories[product.category]) {
        categories[product.category] = [];
      }
      categories[product.category].push(product);
    }
    const rows = [];
    for (let category in categories) {
      rows.push(<ProductCategoryRow name={category} key={'category-' + category} />);
      for (let product of categories[category]) {
        if (this.props.showOnlyInStock && (! product.stocked)) {
          continue;
        }
        if (this.props.query && (! product.name.toLowerCase().includes(this.props.query.toLowerCase()))) {
          continue;
        }
        rows.push(<ProductRow stocked={product.stocked} name={product.name} price={product.price} key={'product-' + product.name} />);
      }
    }

    return (
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </table>
    );
  }
}

class ProductCategoryRow extends React.Component {
  render() {
    return (
      <tr colSpan="2"><th>{this.props.name}</th></tr>
    );
  }
}

class ProductRow extends React.Component {
  render() {
    return (
      <tr className={this.props.stocked ? 'stocked' : 'outOfStock'}>
        <th>{this.props.name}</th>
        <td>{this.props.price}</td>
      </tr>
    );
  }
}

ReactDOM.render(
  <FilterableProductTable />,
  document.getElementById('thinking-in-react')
);
